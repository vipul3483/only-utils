def snake_case_to_camel(word: str) -> str:
    """Convert snake case to camel case.

    Args:
        word(str): Word to convert.

    Returns:
        str: Word in camel case.
    """
    first, *others = word.split('_')
    return ''.join([first.lower(), *map(str.title, others)])
